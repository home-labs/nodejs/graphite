import { AbstractTreeIteratorProtocol } from "./index.mjs";


export class HashMapIteratorProtocol extends AbstractTreeIteratorProtocol<object> {

    constructor(dataStructure: object) {
        super(Object.values(dataStructure));
    }

    isLeafNode(current: any): boolean {
        if (current
            && typeof current === 'object'
            && (
                current.constructor === Object
                || current.constructor === Array
            )
        ) {
            return false;
        }

        return true;
    }

    resolveElements4NextIteration(current: object): object[] {
        return Object.values(current);
    }

}
