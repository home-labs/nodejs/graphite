import { QueueIterator } from "@cyberjs.on/design-protocools/iterator";

import { AbstractTreeIteratorProtocol } from "./index.mjs";


export class TreeIteratorDepthStrategy<T extends object> extends QueueIterator<T> {

    #treeIterator!: AbstractTreeIteratorProtocol<T>

    #stack!: T[];

    constructor(treeIterator: AbstractTreeIteratorProtocol<T>) {
        super(treeIterator.initialQueue);

        this.#treeIterator = treeIterator;

        this.#updateStack();
    }

    override next() {

        let data: { value?: T, done?: boolean } = {};

        let value!: T;

        let elements4NestIteration;

        if (this.isDone()) {
            data = { done: true };
        } else {

            value = this.currentValue;

            data = { value };

            if (this.#treeIterator.isLeafNode(value)) {
                this.increment();
            } else {
                this.#updateStack();

                elements4NestIteration = this.#treeIterator
                    .resolveElements4NextIteration(value);
                this.#redefineElements4NextIteration(elements4NestIteration);

                this.update(this.#stack);
            }

        }

        return data;
    }

    #redefineElements4NextIteration(elements: T[]) {
        this.#putElementsOnStack(elements);
    }

    #updateStack() {
        this.updateRemnantElements();
        this.#stack = this.remnantElements;
    }

    #putElementsOnStack(elements: T[]) {
        this.#stack.unshift(...elements);
    }

}
