export abstract class AbstractTreeIteratorProtocol<T extends object> {

    #_initialQueue!: T[];

    constructor(queue: T[]) {
        this.#_initialQueue = queue;
    }

    abstract isLeafNode(current: any): boolean;

    abstract resolveElements4NextIteration(current: T): T[];

    get initialQueue() {
        return this.#_initialQueue;
    }

}
