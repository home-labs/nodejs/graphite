import { QueueIterator } from "@cyberjs.on/design-protocools/iterator";

import { AbstractTreeIteratorProtocol } from "./index.mjs";


export class TreeIteratorBreadthStrategy<T extends object> extends QueueIterator<T> {

    #treeIterator!: AbstractTreeIteratorProtocol<T>

    #nextQueue: T[];

    constructor(treeIterator: AbstractTreeIteratorProtocol<T>) {
        super(treeIterator.initialQueue);

        this.#treeIterator = treeIterator;

        this.#nextQueue = [];
    }

    override next() {

        let data: { value?: T, done?: boolean } = {};

        let value!: T;

        if (this.isDone()) {
            data = { done: true };
        } else {

            value = this.currentValue;

            data = { value };

            if (!this.#treeIterator.isLeafNode(value)) {
                this.#putElementsOnNextQueue(this
                    .#treeIterator.resolveElements4NextIteration(value));
            }

            if (this.isInLastIteration()) {
                this.#update(this.#nextQueue);
            }

            this.increment();

        }

        return data;
    }

    #update(elements: T[]) {
        super.update(elements);
        this.#nextQueue = [];
    }

    #putElementsOnNextQueue(elements: T[]) {
        this.#nextQueue.push(...elements);
    }

}
