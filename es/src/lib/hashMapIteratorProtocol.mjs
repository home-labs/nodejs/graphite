import { AbstractTreeIteratorProtocol } from "./index.mjs";
export class HashMapIteratorProtocol extends AbstractTreeIteratorProtocol {
    constructor(dataStructure) {
        super(Object.values(dataStructure));
    }
    isLeafNode(current) {
        if (current
            && typeof current === 'object'
            && (current.constructor === Object
                || current.constructor === Array)) {
            return false;
        }
        return true;
    }
    resolveElements4NextIteration(current) {
        return Object.values(current);
    }
}
//# sourceMappingURL=hashMapIteratorProtocol.mjs.map