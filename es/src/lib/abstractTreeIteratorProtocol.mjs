export class AbstractTreeIteratorProtocol {
    #_initialQueue;
    constructor(queue) {
        this.#_initialQueue = queue;
    }
    get initialQueue() {
        return this.#_initialQueue;
    }
}
//# sourceMappingURL=abstractTreeIteratorProtocol.mjs.map