import { QueueIterator } from "@cyberjs.on/design-protocools/iterator";
export class TreeIteratorBreadthStrategy extends QueueIterator {
    #treeIterator;
    #nextQueue;
    constructor(treeIterator) {
        super(treeIterator.initialQueue);
        this.#treeIterator = treeIterator;
        this.#nextQueue = [];
    }
    next() {
        let data = {};
        let value;
        if (this.isDone()) {
            data = { done: true };
        }
        else {
            value = this.currentValue;
            data = { value };
            if (!this.#treeIterator.isLeafNode(value)) {
                this.#putElementsOnNextQueue(this
                    .#treeIterator.resolveElements4NextIteration(value));
            }
            if (this.isInLastIteration()) {
                this.#update(this.#nextQueue);
            }
            this.increment();
        }
        return data;
    }
    #update(elements) {
        super.update(elements);
        this.#nextQueue = [];
    }
    #putElementsOnNextQueue(elements) {
        this.#nextQueue.push(...elements);
    }
}
//# sourceMappingURL=treeIteratorBreadthStrategy.mjs.map