import { QueueIterator } from "@cyberjs.on/design-protocools/iterator";
export class TreeIteratorDepthStrategy extends QueueIterator {
    #treeIterator;
    #stack;
    constructor(treeIterator) {
        super(treeIterator.initialQueue);
        this.#treeIterator = treeIterator;
        this.#updateStack();
    }
    next() {
        let data = {};
        let value;
        let elements4NestIteration;
        if (this.isDone()) {
            data = { done: true };
        }
        else {
            value = this.currentValue;
            data = { value };
            if (this.#treeIterator.isLeafNode(value)) {
                this.increment();
            }
            else {
                this.#updateStack();
                elements4NestIteration = this.#treeIterator
                    .resolveElements4NextIteration(value);
                this.#redefineElements4NextIteration(elements4NestIteration);
                this.update(this.#stack);
            }
        }
        return data;
    }
    #redefineElements4NextIteration(elements) {
        this.#putElementsOnStack(elements);
    }
    #updateStack() {
        this.updateRemnantElements();
        this.#stack = this.remnantElements;
    }
    #putElementsOnStack(elements) {
        this.#stack.unshift(...elements);
    }
}
//# sourceMappingURL=treeIteratorDepthStrategy.mjs.map