import { HashMapIteratorProtocol, TreeIteratorDepthStrategy } from "../lib/index.mjs";
const hashMap = {
    1: '1v',
    2: '2v',
    3: '3v',
    4: {
        4.1: {
            '4.1.1': '4.1.1v',
            '4.1.2': '4.1.2v',
            '4.1.3': '4.1.3v'
        },
        4.2: '4.2v',
        4.3: '4.3v',
        4.4: '4.4v',
        4.5: {
            '4.5.1': '4.5.1v',
            '4.5.2': '4.5.2v',
            '4.5.3': '4.5.3v'
        }
    },
    5: '5v',
    6: '6v',
    7: '7v',
    8: {
        8.1: '8.1v',
        8.2: '8.2v',
        8.3: '8.3v'
    },
    9: [
        '9.1v',
        '9.2v',
        '9.3v',
        {
            9.4: '9.4v',
            9.5: '9.5v',
            9.6: '9.6v',
            9.7: {
                '9.7.1': '9.7.1v',
                '9.7.2': '9.7.2v',
                '9.7.3': '9.7.3v'
            },
            9.8: '9.8v',
            9.9: '9.9v',
            9.10: '9.10v'
        }
    ]
};
console.log('Structure to iterate:', hashMap);
console.log('------------\n');
const hashMapIteratorProtocol = new HashMapIteratorProtocol(hashMap);
const depthIterator = new TreeIteratorDepthStrategy(hashMapIteratorProtocol);
for (let value of depthIterator) {
    console.log(value);
    console.log('------------');
}
//# sourceMappingURL=dfs.mjs.map