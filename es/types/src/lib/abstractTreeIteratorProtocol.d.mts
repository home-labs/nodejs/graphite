export declare abstract class AbstractTreeIteratorProtocol<T extends object> {
    #private;
    constructor(queue: T[]);
    abstract isLeafNode(current: any): boolean;
    abstract resolveElements4NextIteration(current: T): T[];
    get initialQueue(): T[];
}
