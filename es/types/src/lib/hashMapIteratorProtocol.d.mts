import { AbstractTreeIteratorProtocol } from "./index.mjs";
export declare class HashMapIteratorProtocol extends AbstractTreeIteratorProtocol<object> {
    constructor(dataStructure: object);
    isLeafNode(current: any): boolean;
    resolveElements4NextIteration(current: object): object[];
}
