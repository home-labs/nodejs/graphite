import { QueueIterator } from "@cyberjs.on/design-protocools/iterator";
import { AbstractTreeIteratorProtocol } from "./index.mjs";
export declare class TreeIteratorDepthStrategy<T extends object> extends QueueIterator<T> {
    #private;
    constructor(treeIterator: AbstractTreeIteratorProtocol<T>);
    next(): {
        value?: T;
        done?: boolean;
    };
}
